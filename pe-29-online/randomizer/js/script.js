const defaultClasses = {
	container: "randomizer",
	start: ["randomizer__btn", "randomizer__btn--start"],
	reset: ["randomizer__btn", "randomizer__btn--reset"],
	title: "randomizer__title",
};

const students = [
	// add students
];

function myRandomInts(quantity, max) {
	const arr = [];
	while (arr.length < quantity) {
		var candidateInt = Math.floor(Math.random() * max);
		if (arr.indexOf(candidateInt) === -1) arr.push(candidateInt);
	}
	sessionStorage.setItem("students", JSON.stringify(arr));
}

document.addEventListener("DOMContentLoaded", function (event) {
	myRandomInts(students.length, students.length);
});

const studContainer = document.querySelector("#studName");
const reset = document.querySelector("#reset");
const start = document.querySelector("#start");

let i = 0;

start.addEventListener("click", function () {
	const studsIndex = JSON.parse(sessionStorage.getItem("students"));

	// console.log(i + "->" + studsIndex[i] + ", " + students[studsIndex[i]]);
	studContainer.textContent = students[studsIndex[i]];
	i++;

	if (i >= students.length) {
		myRandomInts(students.length, students.length);
		i = 0;
	}
});

reset.addEventListener("click", function () {
	myRandomInts(students.length, students.length);
	i = 0;
});
