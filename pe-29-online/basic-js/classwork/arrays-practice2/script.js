// let recipes = [
//   {
//     name: "Томатний суп",
//     ingredients: ["томати", "вода", "сіль"],
//     instructions: "Змішайте всі інгредієнти та варіть 20 хвилин.",
//   },
// ];

// function addRecipe(name, ingredients, instructions) {
//   let newObj = {
//     name,
//     ingredients,
//     instructions,
//   };

//   recipes.push(newObj);
// }

// addRecipe(
//   "Омлет",
//   ["яйця", "молоко", "сіль"],
//   "Змішайте всі інгредієнти, смажте на сковороді  5-10 хвилин."
// );

// console.log(recipes); //[{}, {}]

// let recipes = [
//   {
//     name: "Томатний суп",
//     ingredients: ["томати", "вода", "сіль"],
//     instructions: "Змішайте всі інгредієнти та варіть 20 хвилин.",
//   },
//   {
//     name: "Омлет",
//     ingredients: ["яйця", "молоко", "сіль"],
//     instructions: "Змішайте всі інгредієнти, смажте на сковороді  5-10 хвилин.",
//   },
//   {
//     name: "Салат",
//     іngredients: ["томати", "огірки", "олія"],
//     instructions: "Змішайте всі інгредієнти. Додайте спеції за смаком.",
//   },
// ];

// function removeRecipe(recipeName) {
//   const index = recipes.findIndex((recipe) => recipe.name === recipeName);
//   if (index !== -1) {
//     initialArray.splice(index, 1);
//     console.log(`Recipe with ${recipeName} is removed`);
//   } else {
//     console.log("Product was not found");
//   }
// }

// function deleteRecipe(initialArray, name) {
//   const filteredProducts = initialArray.filter(
//     (recipe) => recipe.name.toLowerCase() !== name.toLowerCase()
//   );
//   return filteredProducts;
// }

// removeRecipe(recipes, "Омлет");
// console.log(recipes); //[{}, {}]
// console.log(deleteRecipe(recipes, "Омлет"));

// function removeRecipe(recipeName) {
//   const filterProducts = recipes.filter(
//     (item) => item.name.toLowerCase() !== recipeName.toLowerCase()
//   );
//   return filterProducts;
// }
// let result = removeRecipe("Омлет");
// console.log(result);

let recipes2 = [
  {
    name: "Томатний суп",
    ingredients: ["томати", "вода", "сіль"],
    instructions: "Змішайте всі інгредієнти та варіть 20 хвилин.",
  },
  {
    name: "Омлет",
    ingredients: ["яйця", "молоко", "сіль"],
    instructions: "Змішайте всі інгредієнти, смажте на сковороді  5-10 хвилин.",
  },
  {
    name: "Салат",
    ingredients: ["томати", "огірки", "олія"],
    instructions: "Змішайте всі інгредієнти. Додайте спеції за смаком.",
  },
];

// function findRecipesByIngridients(...іngredients) {
//   return recipes.filter((recipe) => {
//     return іngredients.every((іngredient) =>
//       recipe.іngredients.includes(іngredient)
//     );
//   });
// }
function findRecipeByIngredients(...ingredients) {
  return recipes2.filter((recipe) =>
    ingredients.every((ingredient) => {
      const ingredientsArr = recipe.ingredients.map((item) =>
        item.toLowerCase()
      );
      return ingredientsArr.includes(ingredient.toLowerCase());
    })
  );
}

// console.log("filtered ingredients", findRecipeByIngredients("молоко", "яйця"));

// function getListOfIngridients(recipeName) {
//   return recipes2
//     .filter((item) => item.name === recipeName)[0]
//     .ingredients.join(", ");
// }

// console.log(getListOfIngridients("Салат")); // "томати", "вода", "сіль"

function getListofIngridients(recipeName) {
  let recipe = recipes2.filter(
    (recipe) => recipe.name.toLowerCase() === recipeName.toLowerCase()
  );
  console.log("recipe", recipe);
  console.log("recipe[0]", recipe[0]);
  return recipe[0].ingredients.join(", ");
}

console.log("list of ingridients", getListofIngridients("Салат"));

// console.log(findRecipesByIngridients("молоко", "яйця"));

// const vegetarianRecipes = [
//   {
//     name: "Вегетаріанський бургер",
//     ingredients: ["булочка", "соєвий котлет", "салат", "томат", "соус"],
//   },
//   {
//     name: "Салат з авокадо",
//     ingredients: ["авокадо", "помідори", "огірки", "олія", "лимонний сік"],
//   },
// ];

// const meatRecipes = [
//   { name: "Стейк", ingredients: ["яловичина", "сіль", "перець"] },
//   {
//     name: "Курячий суп",
//     ingredients: ["курка", "вода", "морква", "цибуля", "сіль"],
//   },
// ];

// const combinedRecipes = []; ???
