// // words count
// // sentens count
// // avarage word lendth
// // most frequent word

// function analyzeText(text) {
//   if (!text || text.trim().length === 0) {
//     console.log("Please, enter some text");
//     return;
//   }

//   // words count
//   const regExForWord = /\b[\w']+\b/g;
//   const words = text.match(regExForWord) || [];
//   const wordsCount = words.length;

//   // sentens count
//   const regExForSentence = /[.!?]+/g; //[\s$]
//   const sentences = text.match(regExForSentence) || [];
//   const sentenceCount = sentences.length;

//   // avarage word lendth
//   let totalLength = 0;
//   for (let i = 0; i < words.length; i++) {
//     totalLength += words[i].length;
//   }

//   let avarageWordLendth = words.length > 0 ? totalLength / wordsCount : 0;

//   // most frequent word
//   let wordFrequency = {};
//   for (let i = 0; i < words.length; i++) {
//     let lowerCaseWord = words[i].toLowerCase();
//     if (!wordFrequency[lowerCaseWord]) {
//       wordFrequency[lowerCaseWord] = 1;
//     } else wordFrequency[lowerCaseWord]++;
//   }

//   let maxFrequency = 0;
//   let maxFrequencyWord = "";

//   for (const word in wordFrequency) {
//     if (wordFrequency[word] > maxFrequency) {
//       maxFrequency = wordFrequency[word];
//       maxFrequencyWord = word;
//     }
//   }

//   //results
//   console.log("Words Count:", wordsCount);
//   console.log("Sentences Count:", sentenceCount);
//   console.log("Avarage Word Lendth:", avarageWordLendth.toFixed(2));
//   console.log(
//     "Most frequent word is:",
//     maxFrequencyWord,
//     "With frequency:",
//     maxFrequency
//   );
// }

// let text = "Hello! How are you? Let's try to analyze text. Text, text, text.";
// analyzeText(text);

// get event date
// validatidation (event name, date, event description)
// result formatting

// function validatidateEventName(eventName) {
//   const regEx = /^[A-Za-z\s]{3,}$/;
//   let result = regEx.test(eventName);
//   return result;
// }

// function validatidateEventDescription(description) {
//   return description.length <= 50;
// }

// function validatidateEventDate(dateStr) {
//   const eventDate = new Date(dateStr);
//   console.log(eventDate);
//   const now = new Date();
//   return eventDate > now;
// }

// function collectEventData() {
//   let eventName = prompt("Please, enter the event name");
//   while (!validatidateEventName(eventName)) {
//     console.log("Incorrect event name");
//     eventName = prompt("Please, enter the event name");
//   }

//   let eventDate = prompt(
//     "Please, enter the date of the event in format: YYYY-MM-DD HH:MM"
//   );
//   while (!validatidateEventDate(eventDate)) {
//     console.log("Try one more time. Incorrect event date");
//     eventDate = prompt(
//       "Please, enter the date of the event in format: YYYY-MM-DD HH:MM"
//     );
//   }

//   let eventDescription = prompt("Please, enter the event description");
//   while (!validatidateEventDescription(eventDescription)) {
//     console.log("Event decription is too long");
//     eventDescription = prompt("Please, enter the event description");
//   }

//   console.log("\n*** Info about the event ***");
//   console.log("Event name:", eventName);
//   console.log("Event date:", new Date(eventDate).toLocaleString()); // 11/04/2024
//   console.log("Event description:", eventDescription);
// }

// collectEventData();

function isLeapYear(year) {
  const testDate = new Date(year, 1, 29);
  const monthIndex = testDate.getMonth();
  return monthIndex === 1;
}

isLeapYear(2024); //true
isLeapYear(2023); //false
