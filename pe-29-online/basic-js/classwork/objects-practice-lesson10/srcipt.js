// function Product(name, category, quantity, price) {
//   this.name = name;
//   this.category = category;
//   this.quantity = quantity;
//   this.price = price;

//   this.describe = function () {
//     console.log(
//       `${this.name}: ${this.category}, quantuty: ${this.quantity}, price: ${this.price}`
//     );
//   };
// }

// const laptop = new Product("Ноутбук", "Електроніка", 50, 20000);
// const book = new Product("Книга", "Книжки", 100, 300);

// console.log("laptop", laptop);
// console.log("book", book);

// laptop.describe();
// book.describe();

// function Product(name, quantity, price) {
//   this.name = name;
//   this.quantity = quantity;
//   this.price = price;

//   this.sell = function (amount) {
//     if (this.quantity < amount) {
//       console.log("Недостатня кількість товару");
//       return;
//     }
//     this.quantity -= amount;
//     console.log(
//       `Товар ${this.name} продано, потокна кількість: ${this.quantity}`
//     );
//   };

//   this.add = function (amount) {
//     this.quantity += amount;
//     console.log(
//       `Товар ${this.name} додано на склад, потокна кількість: ${this.quantity}`
//     );
//   };
// }

// const apples = new Product("Яблука", 10, 100);
// const oranges = new Product("Апельсин", 12, 50);

// console.log(apples);

// function Product(name, quantity, price) {
//   this.name = name;
//   this.quantity = quantity;
//   this.price = price;
// }

// Product.prototype.sell = function (amount) {
//   if (this.quantity < amount) {
//     console.log("Недостатня кількість товару");
//     return;
//   }
//   this.quantity -= amount;
//   console.log(
//     `Товар ${this.name} продано, потокна кількість: ${this.quantity}`
//   );
// };

// Product.prototype.add = function (amount) {
//   this.quantity += amount;
//   console.log(
//     `Товар ${this.name} додано на склад, потокна кількість: ${this.quantity}`
//   );
// };

// const apples = new Product("Яблука", 10, 100);
// const oranges = new Product("Апельсин", 12, 50);

// apples.sell(10);
// oranges.add(2);

// console.log(apples);
// console.log(apples.hasOwnProperty("name"));
// console.log(apples.hasOwnProperty("sell"));

// console.log("name" in apples);
// console.log("sell" in apples);

// class Product {
//   constructor(name, quantity, price) {
//     this.name = name;
//     this.quantity = quantity;
//     this.price = price;
//   }

//   sell(amount) {
//     if (this.quantity < amount) {
//       console.log("Недостатня кількість товару");
//       return;
//     }
//     this.quantity -= amount;
//     console.log(
//       `Товар ${this.name} продано, потокна кількість: ${this.quantity}`
//     );
//   }

//   add(amount) {
//     this.quantity += amount;
//     console.log(
//       `Товар ${this.name} додано на склад, потокна кількість: ${this.quantity}`
//     );
//   }
// }

// const banana = new Product("Банан", 20, 200);
// const apple = new Product("Яблука", 50, 100);

// banana.sell(10);
// banana.sell(5);
// banana.add(5);

// console.log(banana);

// Object.keys();
// const car = {
//   make: "Ford",
//   model: "Mustang",
//   year: 2000,
// };

// let keys = Object.keys(car);
// console.log(keys); // ['make', 'model', 'year']

// // Object.values();
// let values = Object.values(car);
// console.log(values); // ["Ford", "Mustang", 2000];

// // Object.entries();
// let entries = Object.entries(car);
// // console.log(entries);

// for (const [key, value] of entries) {
//   // console.log(`${element[0]}: ${element[1]}`);
//   console.log(`${key}: ${value}`);
// }
