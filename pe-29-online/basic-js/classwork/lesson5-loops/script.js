// syntax for loop
// for (ініціалізація; умова; крок) {
// блок коду, який ми виконуємо
// }
// for (; ;) {}

// example 1
// for (let i = 0; i <= 5; i++) {
//   console.log(i);
// }

// example 2
// let a = 1;

// for (; a <= 5; a++) {
//   let b = a + 10;
//   console.log(b);
// }

// example 3
// for (;;) { // Infinity loop
//   console.log("Hello!");
// }

// example 4
// for (let c = 0; c < 3; console.log(c++));

//example 5
// for (let i = 1; i <= 10; i++) {
//   if (i % 2 === 0) {
//     console.log(i);
//   }
// }

// syntax while loop
// while (умова) {
//   // блок коду, який ми виконуємо
// }

//example 1
// let d = 1;

// while (d <= 5) {
//   console.log(d);
//   d++;
// }

//example 2
// let num = 1;

// while (num <= 10) {
//   console.log(num);
//   num += 2;
// }

// syntax do while loop
// do {
//   // блок коду, який ми виконуємо
// } while (умова);

//example
// let input;

// do {
//   input = prompt('Введить "вихід" для завершення');
//   console.log(`Ви ввели ${input}`);
// } while (input !== "вихід");

//break operator

// example with for loop
// for (let count = 0; count < 10; count++) {
//   if (count === 5) {
//     break;
//   }
//   console.log(count);
// }

// example with while loop
// let counter = 0;

// while (counter < 10) {
//   if (counter === 5) {
//     break;
//   }
//   console.log(counter);
//   counter++;
// }

// example with do while loop

// let nextCounter = 0;

// do {
//   if (nextCounter === 5) {
//     break;
//   }
//   console.log(nextCounter);
//   nextCounter++;
// } while (nextCounter < 10);

//continue operator
// example with for loop
// for (let count = 0; count < 10; count++) {
//   if (count === 5) {
//     continue;
//   }
//   console.log(count);
// }

// example with while loop
// let i = 0;

// while (i < 10) {
//   i++;
//   if (i % 2 !== 0) {
//     continue;
//   }
//   console.log(i);
// }

// вкладені цикли for
// for (let i = 1; i <= 10; i++) {
//   for (let j = 1; j <= 10; j++) {
//     console.log(`${i} * ${j} = ${i * j}`);
//   }
// }

// вкладені цикли while
// let i = 1;

// while (i <= 10) {
//   let j = 1;

//   while (j <= 10) {
//     console.log(`${i} * ${j} = ${i * j}`);
//     j++;
//   }

//   i++;
// }

// for (let prop in object) {
//   //code
// }

// const person = {
//   name: "Anna",
//   age: 24,
//   job: "FE dev",
// };

// for (let key in person) {
//   console.log(`${key}: ${person[key]}`);
// }

// for (let value of itarable) {
//   //code
// }

// const fruits = ["apple", "orange", "banana"];
// for (let fruit of fruits) {
//   console.log(fruit);
// }
