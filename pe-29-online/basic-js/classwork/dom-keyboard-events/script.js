// document.addEventListener("keydown", function (event) {
//   console.log(
//     `User select btn with code: ${event.code}, with key: ${event.key}`
//   );
// });

// document.addEventListener("keyup", function (event) {
//   console.log(`Key code: ${event.code}, key: ${event.key}`);
// });

// document
//   .getElementById("first-input")
//   .addEventListener("keydown", function (event) {
//     if (event.key === "Enter") {
//       event.preventDefault();
//       console.log(event.key);
//       console.log("Ви натиснули на Enter без відправлення форми");
//     }
//   });

// document.addEventListener("scroll", function () {});

// const navbar = document.getElementById("navbar");

// window.addEventListener("scroll", function () {
//   let scrollTop = document.documentElement.scrollTop; //in px
//   let scrollLeft = document.documentElement.scrollLeft; //in px

//   console.log(scrollLeft);

//   if (scrollTop > 50) {
//     navbar.style.backgroundColor = "rgba(0, 0, 0, 0.8)";
//   } else {
//     navbar.style.backgroundColor = "rgba(0, 0, 0, 0.5)";
//   }
// });

// const form = document.forms.first;
// const inputEl = form.elements.username;
// inputEl.value = "Ihor";
// console.log(inputEl.value);

// const lastNameInput = document.querySelector('input[name="lastName"]');

// const form = lastNameInput.form;

// form.addEventListener("submit", function (event) {
//   console.log(`Форма відправлена, ім'я: ${form.firstName.value}`);
//   event.preventDefault();
// });

// const select = document.getElementById("select");

// select.options[2].selected = true;
// select.value = "pear";
// select.selectedIndex = 2;

// const select = document.getElementById("select");

// const mango = new Option("Mango", "mango", false, true);

// select.add(mango);

const form = document.getElementById("registration");

// form.elements.firstName.addEventListener("focus", function () {
//   console.log("Елемент у фокусі");
// });
// form.elements.firstName.addEventListener("blur", function () {
//   console.log("Фокус з елемента прибраний");
// });

form.elements.firstName.addEventListener("change", function (event) {
  console.log(event.target.value);
});

form.elements.firstName.addEventListener("input", function (event) {
  console.log(event.target.value);
});
