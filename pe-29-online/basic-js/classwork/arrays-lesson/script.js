// let fruits = ["Apple", "Orange", "Melon"];
// console.log(fruits);
// console.log(fruits[0]); // Apple
// console.log(fruits[1]); // Orange
// console.log(fruits[2]); // Melon
// fruits[2] = "Pear";
// console.log(fruits);
// console.log(fruits[2]); // Pear
// fruits[10] = "Melon";
// console.log(fruits);
// console.log(fruits[10]); // Melon
// console.log(fruits.length); //11
// console.log(fruits[fruits.length - 1]); //Melon

// let colors = new Array("red", "green", "blue");
// // console.log(colors);

// let fiveElements = new Array(5);
// // console.log("fiveElements", fiveElements);

// for (let i = 0; i < fruits.length; i++) {
//   console.log(fruits[i]); // Apple, Orange, Melon
// }

// for (let fruit of fruits) {
//   console.log(fruit); // Apple, Orange, Melon
// }

// fruits.forEach(function (item, index, arr) {
//   console.log(index, item, arr);
// });

// let matrix = [
//   [1, 2, 3],
//   [4, 5, 6],
//   [7, 8, 9],
// ];

// console.log(matrix[0][0]); //1
// console.log(matrix[1][1]); //5

// let fruits1 = ["Apple", "Orange", "Melon"];
// let fruits2 = ["Apple", "Orange", "Melon"];
// // console.log(fruits1 === fruits2); //false

// function arraysEqual(arr1, arr2) {
//   if (arr1.length !== arr2.length) {
//     return false;
//   }

//   for (let i = 0; i < arr1.length; i++) {
//     if (arr1[i] !== arr2[i]) {
//       return false;
//     }
//   }

//   return true;
// }

// // console.log(arraysEqual(fruits1, fruits2)); //true

// function arraysEqualByJson(arr1, arr2) {
//   return JSON.stringify(arr1) === JSON.stringify(arr2);
// }

// console.log(arraysEqualByJson(fruits1, fruits2)); //true

// //We can use library Lodash with isEqual(arr1, arr2) method

//push
// let fruits = ["Apple", "Orange", "Melon"];
// let newLength = fruits.push("Pear", "Banana"); //newLength

// console.log("newLength", newLength);
// console.log("arr", fruits);

//pop
// let deletedEl = fruits.pop(); //"Banana"
// console.log("deletedEl", deletedEl);
// console.log("arr", fruits);

//unshift
// let fruits1 = ["Apple", "Orange", "Melon"];
// let newLengthOfFruits1 = fruits1.unshift("Banana");
// console.log("newLengthOfFruits1", newLengthOfFruits1);
// console.log("arr", fruits1);

//shift()
// let deletedEl = fruits1.shift(); //"Banana"
// console.log("deletedEl", deletedEl);
// console.log("arr", fruits1);

//splice
// let fruits = ["Apple", "Orange", "Melon"];
// fruits.splice(1, 2, "Banana", "", "", "");
// console.log(fruits);

// let nums = [1, 2, 3, 4, 5];
// console.log(nums.slice(1, 3));

// let numbers = [10, 11, 12, 13, 14];
// console.log(nums.concat(numbers));

// console.log(
//   nums.map(function (element) {
//     return element * 2;
//   })
// );

// console.log(
//   nums.filter(function (element) {
//     return element % 2 === 0;
//   })
// );

// console.log(
//   nums.find(function (element) {
//     return element % 2 === 0;
//   })
// );

// console.log(nums.indexOf(4)); // 3
// console.log(nums.includes(4)); // true

// let nums = [2, 3, 1, 5, 4, 6];
// // console.log(nums.sort().reverse());

// // every();
// // some();

// console.log(
//   nums.every(function (element) {
//     return element % 2 === 0;
//   })
// ); // false

// console.log(
//   nums.some(function (element) {
//     return element % 2 === 0;
//   })
// ); // true

//reduce

// ... rest
function sum(...nums) {
  return nums.reduce(function (acc, curr) {
    return acc + curr;
  }, 0);
}
console.log(sum(1, 2, 3, 4, 5, 6, 7));

// ... spread
let nums1 = [1, 2, 3];
let nums2 = [4, 5, 6, 10];

console.log([...nums1, ...nums2]);

let copyOfArrNum2 = [...nums2];
