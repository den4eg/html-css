let score = 75;

if (score > 75) {
  console.log("Вітаємо, ви склали іспит");
} else {
  console.log("Ви не склали іспит");
}

let password = "secret";

if (password === "secret") {
  console.log("Доступ  дозволено");
} else {
  console.log("Пароль не вірний");
}

let rating = 8;

if (rating > 9) {
  console.log("Відмінний фільм");
} else if (rating >= 7) {
  console.log("Хороший фільм");
} else if (rating >= 5) {
  console.log("Задовільний фільм");
} else {
  console.log("Поганий фільм");
}

let month = "ЛИпень".toLowerCase(); //'липень'

if (month === "грудень" || month === "січень" || month === "лютий") {
  console.log("Зима");
} else if (month === "березень" || month === "квітень" || month === "травень") {
  console.log("Весна");
} else if (month === "червень" || month === "липень" || month === "серпень") {
  console.log("Літо");
} else if (
  month === "вересень" ||
  month === "жовтень" ||
  month === "листопад"
) {
  console.log("Осінь");
} else {
  console.log("Невірно введений місяць");
}

// if () {
//   if () {
//     if () {

//     }
//   }
// }

// switch (вираз) {
//   case значення1:
//     //блок коду, що виконується, коли вираз === значення1
//     break;
//   case значення2:
//     //блок коду, що виконується, коли вираз === значення2
//     break;
//   case значення3:
//     //блок коду, що виконується, коли вираз === значення3
//     break;
//   default: //optional
//   //блок коду, який виконається, якщо жоден case не відповідає значеню вираз
// }

let day = 3;

switch (day) {
  case 1:
    console.log("Пн");
    break;

  case 2:
    console.log("Вт");
    break;

  case 3:
    console.log("Ср");
    break;

  case 4:
    console.log("Чт");
    break;

  case 5:
    console.log("Пт");
    break;

  case 6:
    console.log("Сб");
    break;

  case 7:
    console.log("Нд");
    break;

  default:
    console.log("Невірний номер дня");
}

let action = "save";

switch (action) {
  case "save":
  case "update":
    console.log("Save or Update the page");
    break;

  case "datele":
    console.log("Delete the file");
    break;

  default:
    console.log("Unknown action");
}

let dataType = typeof true;

console.log(dataType); // "boolean"

switch (dataType) {
  case "boolean":
    console.log("This is Boolean");
    break;
  case "string":
    console.log("This is String");
    break;
  case "number":
    console.log("This is number");
    break;

  default:
    console.log("Unknown type");
}

// умова ? вираз1 : вираз2;

let age = 15;

let isAdult = age >= 18 ? "Дороcлий" : "Неповнолітній";
console.log(isAdult);

let studentScore = 85;
let grade =
  studentScore >= 90
    ? "A"
    : studentScore >= 80
    ? "B"
    : studentScore >= 70
    ? "C"
    : studentScore >= 60
    ? "D"
    : "F";

let isUserLoggedIn = false;

isUserLoggedIn
  ? console.log("Вітаю на сайті!")
  : console.log("Будь ласка, увійдть в систему!");

let userName = null;
let displayName = userName ? userName : "Гість";

console.log(displayName);

// let password = "secret";

// if (password === "secret") {
//   console.log("Доступ  дозволено");
// } else {
//   console.log("Пароль не вірний");
// }

password === "secret"
  ? console.log("Доступ  дозволено")
  : console.log("Пароль не вірний");

// let month = "ЛИпень".toLowerCase(); //'липень'

// if (month === "грудень" || month === "січень" || month === "лютий") {
//   console.log("Зима");
// } else if (
//   month === "березень" ||
//   month === "квітень" ||
//   month === "травень"
// ) {
//   console.log("Весна");
// } else if (month === "червень" || month === "липень" || month === "серпень") {
//   console.log("Літо");
// } else if (
//   month === "вересень" ||
//   month === "жовтень" ||
//   month === "листопад"
// ) {
//   console.log("Осінь");
// } else {
//   console.log("Невірно введений місяць");
// }

let currentMonth = "Липень";

switch (currentMonth.toLowerCase()) {
  case "грудень":
  case "січень":
  case "лютий":
    console.log("Зима");
    break;
  case "березень":
  case "квітень":
  case "травень":
    console.log("Весна");
    break;
  case "червень":
  case "липень":
  case "серпень":
    console.log("Літо");
    break;
  case "вересень":
  case "жовтень":
  case "листопад":
    console.log("Осінь");
    break;
  default:
    console.log("місяць вказано не вірно");
}
