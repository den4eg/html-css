// Switch case
// Разом Простий калькулятор

// function simpleCalculator() {
// let operation = prompt("Chouse the operation: +, -, *, /");

// let firstNumber = prompt("Enter first value");
// let secondNumber = prompt("Enter second value");

// firstNumber = parseFloat(firstNumber);
// secondNumber = parseFloat(secondNumber);

// let result;

// switch (operation) {
//   case "+":
//     result = firstNumber + secondNumber;
//     break;

//   case "-":
//     result = firstNumber - secondNumber;
//     break;

//   case "*":
//     result = firstNumber * secondNumber;
//     break;

//   case "/":
//     if (secondNumber === 0) {
//       // debugger;
//       alert("We can not devide on 0");
//       // return;
//     } else {
//       result = firstNumber / secondNumber;
//     }
//     break;

//   default:
//   // alert("Unknown operation");
//   // return;
// }

// alert(`The result is ${result}`);
// }
// simpleCalculator();

// Разом Простий калькулятор з повторенням
// let isRepeat;

// do {
//   let operation = prompt("Chouse the operation: +, -, *, /");

//   let firstNumber = prompt("Enter first value");
//   let secondNumber = prompt("Enter second value");

//   firstNumber = parseFloat(firstNumber);
//   secondNumber = parseFloat(secondNumber);

//   let result;

//   switch (operation) {
//     case "+":
//       result = firstNumber + secondNumber;
//       break;

//     case "-":
//       result = firstNumber - secondNumber;
//       break;

//     case "*":
//       result = firstNumber * secondNumber;
//       break;

//     case "/":
//       if (secondNumber === 0) {
//         // debugger;
//         alert("We can not devide on 0");
//         // return;
//       } else {
//         result = firstNumber / secondNumber;
//       }
//       break;

//     default:
//       alert("Unknown operation");
//   }

//   alert(`The result is ${result}`);
//   isRepeat = prompt("Do you want to continue (yes/no)").toLowerCase() === "yes";
// } while (isRepeat);

// Індивідуальго Перетворення числа в римське

// Виводимо значення у діапазоні введених користувачем чисел (від меньшого до більшого)
// let var1 = prompt("Enter first number");
// let var2 = prompt("Enter second number");

// var1 = parseInt(var1, 10);
// var2 = parseInt(var2, 10);

// if (!isNaN(var1) && !isNaN(var2)) {
//   // let start, end;

//   // if (var1 < var2) {
//   //   start = var1;
//   //   end = var2;
//   // } else {
//   //   start = var2;
//   //   end = var1;
//   // }

//   let start = var1 < var2 ? var1 : var2;
//   let end = var1 > var2 ? var1 : var2;

//   for (let i = start; i <= end; i++) {
//     console.log(i);
//   }
// } else {
//   alert("Incorrect values");
// }

/* Напишіть програму «Кавова машина».
 *
 * Програма приймає монети та готує напої:
 * - Кава за 25 монет;
 * - Капучіно за 50 монет;
 * - Чай за 10 монет.
 *
 * Щоб програма дізналася, що робити, вона повинна знати:
 * - Скільки монет користувач вніс;
 * - Який він бажає напій.
 *
 * Залежно від того, який напій вибрав користувач,
 * програма повинна обчислити здачу та вивести повідомлення в консоль:
 * «Ваш «НАЗВА НАПОЇ» готовий. Візьміть здачу: "СУМА ЗДАЧІ".".
 *
 * Якщо користувач ввів суму без здачі - вивести повідомлення:
 * «Ваш «НАЗВА НАПОЇ» готовий. Дякую за суму без здачі! :)
 */

let deposit = 0;

while (true) {
  let drink = prompt(
    "Кава (25 монет), Капучіно (50 монет), Чай (10 монет)"
  ).toLowerCase();
  let coins = parseInt(prompt("Введіть кількість монет"), 10) + deposit;
  let price;

  // if (drink === "кава") {
  //   price = 25;
  // } else if (drink === "капучіно") {
  //   price = 50;
  // } else if (drink === "чай") {
  //   price = 10;
  // } else {
  //   alert("Будь ласка, оберіть доступний напій");
  //   continue;
  // }

  switch (drink) {
    case "кава":
      price = 25;
      break;

    case "капучіно":
      price = 50;
      break;

    case "чай":
      price = 10;
      break;

    default:
      alert("Будь ласка, оберіть доступний напій");
      continue;
  }

  if (coins >= price) {
    let change = coins - price;
    if (change > 0) {
      alert(`Ваш напій ${drink} готовий. Ваша решта: ${change}`);
    } else {
      alert(`Ваш напій ${drink} готовий`);
    }
    break;
  } else {
    deposit = coins;
    // let coffePrice = 25 - deposit;
    // let cuppuchPrice = 50 - deposit;
    // let teaPrice = 10 - deposit;
    alert(`Недостатня сумма для оплати. Ваш депозит: ${deposit}`);
  }
}
