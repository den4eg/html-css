// Яка різниця між подіями `keydown` та `keyup`?
// Яка подія буде виконуватися, якщо ми затиснули клавіатуру?

// Яка подія відбувається, коли значення текстового поля змінюється та втрачає фокус?

// Як можна запобігти всплиттю події в DOM?

// Що робить метод event.preventDefault()

// Створити текстове поле, в якому користувач зможе бачити, які клавіші вони натискають
// Задача: Написати JavaScript, який виводить на екран символ клавіші, яка була натиснута
// const myInput = document.getElementById("input");

// myInput.addEventListener("keydown", function (event) {
//   const pressedKey = event.key;
//   const paragraph = document.createElement("p");
//   paragraph.textContent = `Користувач натиснув: ${pressedKey}`;
//   myInput.after(paragraph);
// });

// document.getElementById("input").addEventListener("keydown", function (event) {
//   document.getElementById(
//     "output"
//   ).textContent = `Користувач натиснув: ${event.key}`;
// });

// Реалізація вкладок (tabs) з динамічним завантаженням вмісту
// const buttons = document.querySelectorAll(".tab-btn");
// let curIndex = 0;

// buttons.forEach((button, index) => {
//   button.addEventListener("click", function () {
//     curIndex = index;
//     setActiveContent(this.getAttribute("data-content"));
//   });

//   button.addEventListener("keydown", function (event) {
//     if (event.key === "ArrowRight") {
//       curIndex = (curIndex + 1) % buttons.length;
//       buttons[curIndex].click();
//     } else if (event.key === "ArrowLeft") {
//       curIndex = (curIndex - 1 + buttons.length) % buttons.length;
//       buttons[curIndex].click();
//     }
//   });
// });

// function setActiveContent(contentId) {
//   const contentWrapper = document.getElementById(contentId);
//   contentWrapper.innerHTML = generateContent(contentId);
//   updateDispay(contentId);
// }

// function generateContent(contentId) {
//   switch (contentId) {
//     case "content1":
//       return "<h2>Новини</h2><p>Тут ви бачите останні новини</p>";
//     case "content2":
//       return "<h2>Галерея</h2><p>Тут ви бачите галерею зображень</p>";
//     case "content3":
//       return "<h2>Контакти</h2><p>Тут ви бачите активну карту</p>";
//   }
// }

// function updateDispay(contentId) {
//   document.querySelectorAll(".tab-content").forEach((contantWrapper) => {
//     if (contantWrapper.id === contentId) {
//       contantWrapper.classList.add("active-tab");
//     } else {
//       contantWrapper.classList.remove("active-tab");
//     }
//   });
// }

// Демонстрація захоплення та всплиття подій

const list = document.querySelector("#todoList ul");

list.addEventListener(
  "click",
  function (event) {
    console.log(event.target.tagName);
    if (event.target.tagName === "LI") {
      event.target.classList.toggle("selected");
    }
  },
  false
);

document.getElementById("todoList").addEventListener(
  "click",
  function () {
    console.log("Подія захоплена на рівні контейнеру");
  },
  true
);

document.getElementById("clear-btn").addEventListener("click", function () {
  document.querySelectorAll(".selected").forEach((li) => {
    li.classList.remove("selected");
  });
});
