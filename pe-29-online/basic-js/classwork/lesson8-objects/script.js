// let emptyObj = {};
// let person = {
//   name: "Anna",
//   age: 24,
//   "likes programming": true,
// };

//dot notation
// syntax object.propertyName

// let book = {
//   title: "Big Data",
//   year: 2020,
// };

// console.log(book.title);
// console.log(book.year);

//object.propName = newValue

// let user = {
//   name: "Anna",
//   age: 20,
// };

// user.age = 24;

// console.log(user.name);
// console.log(user.age);

//object.newProperty = value
// let user = {
//   name: "Alex",
// };

// user.age = 30;
// user.proffesion = "BE";

// console.log(user);

//delete object.property

//bracket notation
//syntax object["propertyName"]

// let book = {
//   title: "Big Data",
//   year: 2020,
//   "is about programming": true,
// };

// let propName = "testTitle";

// console.log(book["title"]);
// console.log(book["year"]);
// console.log(book["is about programming"]);
// console.log(book[propName]);

//syntax object["propertyName"] = newValue

// let user = {
//   name: "Anna",
//   age: 20,
//   "likes reading": true,
// };

// user["likes reading"] = false;
// console.log(user["likes reading"]);

//object['newProperty'] = value

// let user = {
//   name: "Alex",
// };

// user["age"] = 30;
// user["proffesion"] = "BE";
// user["is proffesional"] = true;

// console.log(user);

// delete user["is proffesional33838"];

// console.log(user);

//delete object['property']

//check property
//in
//syntax "pripertyName" in object
// const car = {
//   make: "Toyota",
//   model: "Camry",
// };

// console.log("model" in car); //true
// console.log("isModelS" in car); //false

//hasOwnProperty()
//syntax object.hasOwnProperty()

// const car = {
//   make: "Toyota",
//   model: "Camry",
// };

// console.log(car.hasOwnProperty("model")); //true
// console.log(car.hasOwnProperty("year")); //false

//is undefined
//syntax object.propertyName !== undefined

// const car = {
//   make: "Toyota",
//   model: "Camry",
//   // text: undefined,
// };

// console.log(car.make !== undefined); //true
// console.log(car.text !== undefined); //false

// constructor function
// function Person(name, age) {
//   this.name = name;
//   this.age = age;

//   this.greet = function () {
//     console.log(`Hello, ${this.name}`);
//   };
// }

// const person1 = new Person("Anna", 24);
// const person2 = new Person("Alex", 30);

// console.log(person1.greet());
// console.log(person2.greet());

// Object.create()
// Object.create(proto, propObject);

// const animal = {
//   isAlive: true,
//   describe: function () {
//     console.log(`This animal is alive: ${this.isAlive}`);
//   },
//   test: {
//     name: "Any",
//   },
// };

// const dog = Object.create(animal);
// dog.describe();

// const cat = Object.create(animal, {
//   name: {
//     value: "Murka",
//     writable: true, //false,
//     enumerable: true,
//     configurable: true,
//   },
// });

// console.log(cat);

// ES6 classes

// class ClassName {
//   constructor(prop1, prop2) {}
// }

// const newObj = new ClassName(arg1, arg2);

// class Person {
//   constructor(name, age) {
//     this.name = name;
//     this.age = age;
//   }

//   greet() {
//     console.log(`Hello, ${this.name}`);
//   }
// }

// const person1 = new Person("Anny", 25);
// const person2 = new Person("Oleg", 27);

// person1.greet();
// person2.greet();

// const person = {
//   name: "Alex",
//   greet: function () {
//     console.log(`Hello, ${this.name}`);
//   },
// };

// for in
// for (let property in object) {
// }

const person = {
  name: "Oleg",
  age: 35,
  profession: "developer",
};

for (const key in person) {
  if (person.hasOwnProperty(key)) {
    console.log(`${key}: ${person[key]}`);
  }
}
