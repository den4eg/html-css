// // console.log(window.document);
// console.log(document.documentElement); //html
// console.log(document.body); //body
// console.log(document.head); //head

// document.body.style.backgroundColor = "red";

// console.log(document.body.childNodes);

// console.log(document.body.firstChild);
// console.log(document.body.lastChild);

// Array.from(document.body.childNodes).filter();

// console.log(document.body.children);
// console.log(document.body.firstElementChild.nextElementSibling);
// console.log(document.body.firstElementChild.previousElementSibling);
// console.log(document.body.lastElementChild);

// console.log(document.body.firstElementChild);
// console.log(document.body.firstElementChild.nextElementSibling);
// // console.log(document.body.firstElementChild.nextElementSibling.lastChild);
// console.log(
//   document.body.firstElementChild.nextElementSibling.firstElementChild
//     .nextElementSibling.textContent
// );

// const adamListItem = document.getElementById("adam-name");
// console.log(adamListItem.textContent);

// const listItems = document.getElementsByClassName("list-item");
// console.log(listItems);

// const fullListItems = document.getElementsByTagName("li");
// console.log(fullListItems);

// const listItem = document.querySelector(".list-item");
// console.log(listItem);

// const listItemsCopy = document.querySelectorAll(".list-item");
// console.log(listItemsCopy);

// document.body.innerHTML = "<h1>New HTML content<h1>";
// console.log(document.body.innerHTML);

// document.body.children[0].textContent = "Hew Title";
// console.log(document.body.children[0].textContent);

// document.body.children[0].hidden = true;
// console.log(document.body.children[0].hidden);

// document.querySelector("#adam-name").innerHTML = "<a href=''>Denis</a>";
