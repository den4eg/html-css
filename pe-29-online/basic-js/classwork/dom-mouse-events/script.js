// const button = document.getElementById("myBtn");

// // button.onclick = function (event) {
// //   // alert("Hello! This is message from the HTML-atr event!");
// //   console.log(event);
// // };

// button.addEventListener("click", function (e) {
//   // alert("Hello! This is message from the HTML-atr event!");
//   console.log(e);
// });

// const obj = {
//   handleEvent: function (event) {
//     console.log(event.type);
//   },
// };

// button.addEventListener("click", obj);

// const addEventBtn = document.getElementById("eventButton");
// const eventList = document.getElementById("eventLog");

// function logEvent(event) {
//   const listItem = document.createElement("li");
//   listItem.textContent = `Відбулася подія: ${event.type}`;

//   eventList.append(listItem);
//   console.log(event.which); //1, 2, 3

// }

// addEventBtn.addEventListener("mousedown", logEvent);
// addEventBtn.addEventListener("mouseup", logEvent);
// addEventBtn.addEventListener("click", logEvent);
// addEventBtn.addEventListener("dblclick", logEvent);

// addEventBtn.onclick = function (event) {
//   if (event.altKey && event.shiftKey) {
//     alert("Hooray!");
//   }
// };

// document.body.addEventListener("mousemove", function (event) {
//   console.log(`clientX: ${event.clientX}, clientY: ${event.clientY}`);
// });

// document.body.addEventListener("click", function (event) {
//   console.log(`clientX: ${event.clientX}, clientY: ${event.clientY}`);
//   console.log(`pageX: ${event.pageX}, pageY: ${event.pageY}`);
// });

const parent = document.getElementById("parent");
const child = document.getElementById("child");

parent.addEventListener("mouseover", function (event) {
  console.log(`mouseover: ${event.target.id}`);
});

parent.addEventListener("mouseout", function (event) {
  console.log(`mouseout: ${event.target.id}`);
});

parent.addEventListener("mouseenter", function (event) {
  console.log(`mouseenter: ${event.target.id}`);
});

parent.addEventListener("mouseleave", function (event) {
  console.log(`mouseleave: ${event.target.id}`);
});
