// function startCountdown() {
//   let startTime = 10;

//   const timerElement = document.getElementById("timer");
//   timerElement.textContent = startTime;

//   const intervalId = setInterval(() => {
//     startTime--;
//     timerElement.textContent = startTime;

//     if (startTime <= 0) {
//       clearInterval(intervalId);
//       setTimeout(() => {
//         alert("Час вийшов");
//       }, 0);
//     }
//   }, 500);
// }

// const startBtn = document.getElementById("start-btn");
// startBtn.addEventListener("click", startCountdown);

// const user = [
//   { name: "Anna", age: 15 },
//   { name: "Anton", age: 20 },
//   { name: "Dmytro", age: 35 },
// ];

// function setUserCard() {
//   const cardWrapper = document.getElementById("data-wrapper");

//   user.map((user) => {
//     const userCard = document.createElement("div");
//     const header = document.createElement("h2");
//     const info = document.createElement("p");

//     header.textContent = `User Name: ${user.name}`;
//     info.textContent = `User Age: ${user.age}`;

//     userCard.append(header, info);
//     cardWrapper.append(userCard);
//     console.log("Завантаження карточок завершено");
//   });
// }

// const startBtn = document.getElementById("start-btn");
// const resetBtn = document.getElementById("reset-btn");
// let timerId;

// startBtn.addEventListener("click", () => {
//   // console.log("Before timeout");
//   timerId = setTimeout(setUserCard, 3000);
//   // console.log("After timeout");
// });

// resetBtn.addEventListener("click", () => {
//   if (timerId) {
//     clearTimeout(timerId);
//     console.log("Завантаження карточок було скасовано");
//   }
// });

// const user = { userName: "Anna", age: 23 };

// // localStorage.setItem("username", "John");
// // localStorage.setItem("age", "23");
// localStorage.setItem("user", JSON.stringify(user));

// const storUser = localStorage.getItem("user");
// console.log(JSON.parse(storUser));

// localStorage.removeItem("username");
// localStorage.removeItem("age");

// sessionStorage.setItem();
// sessionStorage.getItem();
// sessionStorage.removeItem();

window.addEventListener("storage", function () {
  console.log("Відбулася подія storage");
});

function changeStorage() {
  localStorage.setItem("message", "Hello!");
}

const changeStorageBtn = document.getElementById("start-btn");
changeStorageBtn.addEventListener("click", () =>
  setTimeout(changeStorage, 3000)
);
