// function createSettingsManager() {
//   let settings = {};

//   function setSetting() {
//     let key = prompt("Введіть назву налаштування профілю:");
//     let value = prompt("Введіть значення налаштування:");
//     settings[key] = value;
//     alert(`Налаштування ${key} зі значення ${value} було додано`);
//   }

//   function getSetting() {
//     let key = prompt("Введіть назву налаштування, яке ви хочете отримати:");
//     let value = settings[key];
//     if (value) {
//       alert(`Значення налаштування:  ${value}`);
//     } else {
//       alert(`Налаштування з назвою ${key} не знайдено`);
//     }
//   }

//   function updateSetting() {
//     let key = prompt("Введіть назву налаштування, яке ви хочете оновити:");
//     let value = settings[key];
//     let isReadyToUpdate = confirm(
//       `Ви впевнені, що хочете оновити значення налаштвання ${key}?`
//     );

//     if (isReadyToUpdate) {
//       if (value) {
//         let newValue = prompt(`Введіть нове значення налаштування ${key}`);
//         settings[key] = newValue;
//         alert(`Налаштування ${key} оновлено до значення ${newValue}`);
//       } else {
//         alert(`Налаштування з назвою ${key} не знайдено`);
//       }
//     } else {
//       return;
//     }
//   }

//   function deleteSetting() {
//     let key = prompt("Введть назву налаштування, яке ви хочете видалити");
//     let value = settings[key];
//     let isReadyToDelete = confirm(
//       `Ви впевненні, що хочете видалити значення налаштування ${key}`
//     );
//     if (isReadyToDelete) {
//       if (value) {
//         delete settings[key];
//         alert(`Значення налаштування ${key} видалено`);
//       } else {
//         alert(`Налаштування з назвою ${key} не знайдено`);
//       }
//     } else {
//       return;
//     }
//   }

//   return {
//     setSetting,
//     getSetting,
//     updateSetting,
//     deleteSetting,
//   };
// }

// const settingsManager = createSettingsManager();
// const settingsManager1 = createSettingsManager();

// settingsManager.setSetting();
// settingsManager.getSetting();
// settingsManager.updateSetting();
// settingsManager.getSetting();
// settingsManager.deleteSetting();
// settingsManager.getSetting();

function createOrderSystem() {
  let order = {
    totalPrice: 0,
    isDiscountApplied: false,
  };

  function addItem(price) {
    // order.totalPrice = order.totalPrice + price;
    order.totalPrice += price;
  }

  function applyDiscount(discountFunction) {
    if (!order.isDiscountApplied) {
      order.totalPrice = discountFunction(order.totalPrice);
      order.isDiscountApplied = true;
    }
  }

  function discribeOrderPrice() {
    console.log(
      `Загальна вартість замовлення: ${
        order.totalPrice
      }. Чи знижка застосована: ${order.isDiscountApplied ? "так" : "ні"}`
    );
  }

  return { addItem, applyDiscount, discribeOrderPrice };
}

const orderSystem = createOrderSystem();

orderSystem.addItem(200);
orderSystem.addItem(150);
orderSystem.discribeOrderPrice();

function tenPercetDiscount(total) {
  return total * 0.9;
}

orderSystem.applyDiscount(tenPercetDiscount);
orderSystem.discribeOrderPrice();

orderSystem.applyDiscount(tenPercetDiscount);
orderSystem.discribeOrderPrice();
