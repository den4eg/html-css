//Function Declaration
// console.log(greet("Anna")); //hoisting

// function greet(name) {
//   return `Hello, ${name}`;
// }

//Function Expression syntax
// const functionName = function(parameters) {
//   func body
// }

// const greet = function (name) {
//   return `Hello, ${name}`;
// };

// console.log(greet("Alex"));

// function add(a, b) {
//   let result = a + b;
//   return result;
// }

// const sum = add(3, 5);
// console.log(sum);

// function divide(a, b) {
//   if (b === 0) {
//     return "You can not divide on 0";
//   }
//   return a / b;
// }

// function checkAge(age) {
//   if (age < 18) {
//     return "Ви неповнолітні";
//   } else if (age >= 18 && age < 60) {
//     return "Ви дорослий";
//   } else {
//     return "Ви пенсіонер";
//   }
// }

// console.log(checkAge(20));

// function getDataType(value) {
//   return typeof value;
// }

// console.log(getDataType(42));
// console.log(getDataType("Hello!"));
// console.log(getDataType({}));
// console.log(getDataType(true));

// function greet(name = "Guest") {
//   return `Hello, ${name}`;
// }

// console.log(greet());

// function sum(...numbers) {
//   // return numbers;
//   return arguments;
// }

// console.log(sum(1, 2, 3, 4, 5));

// let func = (arg1, arg2, ...argN) => expression;
// let func = (arg1, arg2, ...argN) => {
//   //
//   return "result";
// };

// const add = (a, b) => a + b;
// const add = (a, b) => {
//   let result = a + b;
//   return result;
// };
// console.log(add(3, 5));

// function showArguments() {
//   for (let i = 0; i < arguments.length; i++) {
//     console.log(`Argument ${i}: ${arguments[i]}`);
//   }
// }
// showArguments("Hello", "World", 2024, true);

// const showArguments = (...arg) => {

// }

// function getData(callback) {
//   setTimeout(() => {
//     const data = "data";
//     callback(data);
//   }, 1000);
// }

// getData((result) => {
//   console.log(result);
// });

//IIFE syntax

// (function() {
//   //func boy
// })();

(function (a, b) {
  console.log(a + b);
})(5, 3);

// function pow(com, exp) {
//   let result = 1;
//   for (let i = 0; i < exp; i++) {
//     result *= com;
//   }
//   return result;
// }
// console.log(pow(3, 4));

function pow(x, n) {
  return x ** n;
  // return Math.pow(x, n)
}

let x = prompt("Введите число:", "");
let n = prompt("Введите степень:", "");

let result = pow(x, n);

alert(`pow(${x}, ${n}) = ${result}`);
