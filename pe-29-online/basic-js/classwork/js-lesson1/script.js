"use strict";

// let a = "Hello!";
// console.log(a);

// var a = "Hello!";
// console.log(a);

// const a = "Hello!";
// a = "sfvkjdsfvb";
// console.log(a);

let name = `Anna`;
let name1 = "Anna";
let name2 = "Anna";

// let age = 30;

let isOnline = true; // false

let emptyValue = null;

let uninitialized; // undefined

// console.log(null === undefined);
// console.log(typeof NaN);
// console.log(NaN === NaN);

let person = {
  name: "Anna",
  age: 40,
  isOnline: false,
};

let fruits = ["Apple", "Banana", 0, true];

let fruits1 = fruits;

let result = "3" + 2;
// console.log(result);
// console.log("3" - 2);

let age1 = "32";
// console.log(Number(age1));
// String();
// Boolean();

// console.log(true + 1);

let counter = 0;
counter = counter + 1;
counter += 1;
counter = counter - 1;
counter -= 1;
counter = counter * 1;
counter *= 1;
counter = counter / 1;
counter /= 1;
counter = counter % 1;
counter %= 1;

let greeting = "Hello, ";
greeting += "Worlg!";

let b = "3";
b = +b; // 3

// console.log(9 / 2);
// console.log(9 % 2);
// console.log(20 % 7);

// let num = 7;

// console.log(num % 2 === 0 ? "Парне" : "Непарне");

console.log(-10 % 3); // -1
console.log(10 % -3); // 1

let num = 3;
let sum = 20;
num = sum = 100;

console.log(5 > 3); //true
console.log(5 < 3); //false
console.log(5 >= 5); //true
console.log(5 <= 3); //false

console.log("apple" < "banana"); //true
console.log("apple" > "Apple"); //true

// &&, ||, !
// && (AND)
console.log(true && true); //true
console.log(false && true && true && true); //false

// || (OR)
console.log(false || true || false || false); //true
console.log(false || false); //false

// ! (NOT)
console.log(!false); //true
console.log(!true); //false

let age = 11;
let hasPermissions = true;

if (age > 18 && hasPermissions) {
  console.log("Ви можете їхати");
} else {
  console.log("В'ізд заборонено");
}

// template strings
let name3 = "John";
let greeting3 = `Hello, ${name3}`;
console.log(greeting3);
