// "use strict";

// // let a = "Hello!";
// // console.log(a);

// // var a = "Hello!";
// // console.log(a);

// // const a = "Hello!";
// // a = "sfvkjdsfvb";
// // console.log(a);

// let name = `Anna`;
// let name1 = "Anna";
// let name2 = "Anna";

// // let age = 30;

// let isOnline = true; // false

// let emptyValue = null;

// let uninitialized; // undefined

// // console.log(null === undefined);
// // console.log(typeof NaN);
// // console.log(NaN === NaN);

// let person = {
//   name: "Anna",
//   age: 40,
//   isOnline: false,
// };

// let fruits = ["Apple", "Banana", 0, true];

// let fruits1 = fruits;

// let result = "3" + 2;
// // console.log(result);
// // console.log("3" - 2);

// let age1 = "32";
// // console.log(Number(age1));
// // String();
// // Boolean();

// // console.log(true + 1);

// let counter = 0;
// counter = counter + 1;
// counter += 1;
// counter = counter - 1;
// counter -= 1;
// counter = counter * 1;
// counter *= 1;
// counter = counter / 1;
// counter /= 1;
// counter = counter % 1;
// counter %= 1;

// let greeting = "Hello, ";
// greeting += "Worlg!";

// let b = "3";
// b = +b; // 3

// // console.log(9 / 2);
// // console.log(9 % 2);
// // console.log(20 % 7);

// // let num = 7;

// // console.log(num % 2 === 0 ? "Парне" : "Непарне");

// console.log(-10 % 3); // -1
// console.log(10 % -3); // 1

// let num = 3;
// let sum = 20;
// num = sum = 100;

// console.log(5 > 3); //true
// console.log(5 < 3); //false
// console.log(5 >= 5); //true
// console.log(5 <= 3); //false

// console.log("apple" < "banana"); //true
// console.log("apple" > "Apple"); //true

// // &&, ||, !
// // && (AND)
// console.log(true && true); //true
// console.log(false && true && true && true); //false

// // || (OR)
// console.log(false || true || false || false); //true
// console.log(false || false); //false

// // ! (NOT)
// console.log(!false); //true
// console.log(!true); //false

// let age = 11;
// let hasPermissions = true;

// if (age > 18 && hasPermissions) {
//   console.log("Ви можете їхати");
// } else {
//   console.log("В'ізд заборонено");
// }

// // template strings
// let name3 = "John";
// let greeting3 = `Hello, ${name3}`;
// // console.log(greeting3);

// Lesson 2

let b = "-10";
console.log(-b);

let c = "trtr";
console.log(!c); //false

// let age = 20;
// console.log(++age);
// console.log(age);

let age = 20;
console.log(age--);
console.log(age);

console.log(typeof age);

/////////////////////////////////
let a = 25;
a *= 2;
console.log(a); //50

let bit = 8; // 1000
bit <<= 1; // b = b << 1
console.log(bit); // 16 - 10000

let sum = 10 + 5;
console.log(15);

let remainder = 10 % 3;
console.log(remainder); //1

// ==, ===, !=, !==, >, <, >=, <=
let str1 = "Hello";
let str2 = "hello";

console.log(str1 === str2); //false
console.log(str1.toLowerCase() === str2.toLowerCase()); //true

console.log(null == undefined); //true
console.log(null === undefined); //false

console.log(NaN == NaN); //false
console.log(NaN === NaN); //false
console.log(isNaN(NaN)); //true

// &&, ||, !
// && (AND)
console.log(true && true); //true
console.log(false && true && true && true); //false

// || (OR)
console.log(false || true || false || false); //true
console.log(false || false); //false

// !!variable ~ Boolean(variable)

let name; //undefined
console.log("variable name equal to", !!name); //false

console.log(!![]); //true

// короткозамкнена оцінка

console.log(false && "anything"); //false
console.log(1 && "anything"); //"anything"

console.log(true || "anything"); //true
console.log("it is" || "anything"); //"it is"

// ??
console.log(null ?? "it is"); //'it is'

// alert, prompt, confirm

// alert(massage)

// alert(true);

//let result = prompt(title, [default])

// let userName = prompt("What's your name?", "Any");
// console.log(userName);

// let result = confirm(message);

// let isConfirmed = confirm("Чи впевнені ви, що хочете видалити цей файл?");

// if (isConfirmed) {
//   console.log("Файл було видалено");
// } else {
//   console.log("Видалення файлу було скасоване");
// }

// console.log(4 + 3 + "3");

// console.log(!0);
// console.log(!!1);
// console.log(!"");

// console.log("5" * "4" == 20 && "6" - "2" === 4);

console.log(+true == 1 && +false === 0);
