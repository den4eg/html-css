// const taskInput = document.getElementById("taskInput");
// const addTaskBtn = document.getElementById("addTaskBtn");
// const taskList = document.querySelector("#taskList");

// function addTaskToTheTaskList() {
//   const taskName = taskInput.value.trim();
//   if (taskName !== "") {
//     const taskItem = document.createElement("div");
//     taskItem.classList.add("taskItem");

//     const p = document.createElement("p");
//     p.textContent = taskName;

//     const span = document.createElement("span");
//     span.classList.add("cross-btn");
//     span.textContent = "❌";
//     span.addEventListener("click", function () {
//       taskItem.remove();
//     });

//     taskItem.append(p);
//     taskItem.append(span);
//     taskList.append(taskItem);

//     taskInput.value = "";
//   }
// }

// addTaskBtn.addEventListener("click", addTaskToTheTaskList);

const openModalButton = document.getElementById("openModalBtn");
const modalContainer = document.querySelector("#modalContainer");
const closeModalBtn = document.getElementById("closeModalBtn");

openModalButton.addEventListener("click", () => {
  modalContainer.style.display = "block";
});

closeModalBtn.addEventListener("click", () => {
  modalContainer.style.display = "none";
});
