// let products = [
//   {
//     id: 1,
//     name: "Laptop",
//     price: 800,
//     category: "Electronics",
//     description: "High performance laptop",
//     quantity: 14,
//     hasDiscount: true,
//   },
//   {
//     id: 2,
//     name: "Smartphone",
//     price: 500,
//     category: "Electronics",
//     description: "Latest model smartphone",
//     quantity: 30,
//     hasDiscount: false,
//   },
//   {
//     id: 3,
//     name: "Tablet",
//     price: 450,
//     category: "Electronics",
//     description: "Portable and powerful tablet",
//     quantity: 22,
//     hasDiscount: true,
//   },
// ];

// function addProduct(initialArray, product) {
//   const newId =
//     initialArray.reduce((acc, curr) => {
//       return curr.id > acc ? curr.id : acc;
//     }, 0) + 1;

//   const newProduct = { id: newId, ...product };
//   initialArray.push(newProduct);
//   console.log("New product was added");
// }

// console.log(products);
// addProduct(products, {
//   name: "Camera",
//   price: 200,
//   category: "Electronics",
//   description: "Digital camera",
//   quantity: 5,
//   hasDiscount: false,
// });
// addProduct(products, {
//   name: "Camera 200",
//   price: 300,
//   category: "Electronics",
//   description: "Digital camera",
//   quantity: 5,
//   hasDiscount: false,
// });
// console.log(products);

// function removeProduct(initialArray, productId) {
//   const index = initialArray.findIndex((product) => product.id === productId);
//   if (index !== -1) {
//     initialArray.splice(index, 1);
//     console.log(`Product with id ${productId} was deleted`);
//   } else {
//     console.log(`Product not found`);
//   }
// }

// console.log(products);
// removeProduct(products, 2);
// console.log(products);

let allProducts = [
  {
    id: 1,
    name: "Laptop",
    price: 50,
    category: "Electronics",
    description: "High performance laptop",
    quantity: 14,
    hasDiscount: true,
  },
  {
    id: 2,
    name: "Smartphone",
    price: 180,
    category: "Electronics",
    description: "Latest model smartphone",
    quantity: 30,
    hasDiscount: false,
  },
  {
    id: 3,
    name: "Tablet",
    price: 450,
    category: "Electronics",
    description: "Portable and powerful tablet",
    quantity: 22,
    hasDiscount: true,
  },
  {
    id: 4,
    name: "Blender",
    price: 550,
    category: "Appliances",
    description: "Kitchen blender for smoothies",
    quantity: 5,
    hasDiscount: false,
  },
  {
    id: 5,
    name: "Coffee Maker",
    price: 230,
    category: "Appliances",
    description: "Automatic coffee maker",
    quantity: 10,
    hasDiscount: true,
  },
  {
    id: 6,
    name: "Garden Chair",
    price: 75,
    category: "Home & Garden",
    description: "Comfortable garden chair",
    quantity: 4,
    hasDiscount: false,
  },
];

function filterProductsByCategory(initialArray, category) {
  const filteredProducts = initialArray.filter(
    (product) => product.category.toLowerCase() === category.toLowerCase()
  );
  return filteredProducts;
}

// console.log(allProducts);
// console.log(filterProductsByCategory(allProducts, "Electronics"));
// console.log(filterProductsByCategory(allProducts, "Appliances"));
// console.log(filterProductsByCategory(allProducts, "Home & Garden"));

//'asc'
//'desc'

function sortProductsByPrice(initialArray, direction = "asc") {
  const sortedProducts = [...initialArray].sort((a, b) => {
    if (direction === "asc") {
      return a.price - b.price;
    } else if (direction === "desc") {
      return b.price - a.price;
    }
  });
  return sortedProducts;
}

// console.log("initialArray", allProducts);
// console.log("sorted by asc", sortProductsByPrice(allProducts, "asc"));
// console.log("sorted by desc", sortProductsByPrice(allProducts, "desc"));

function filterProductsByCategoryAndPrice(
  initialArray,
  category,
  minPrice,
  maxPrice
) {
  const filteredProducts = initialArray.filter(
    (product) =>
      product.category.toLowerCase() === category.toLowerCase() &&
      product.price >= minPrice &&
      product.price <= maxPrice
  );
  return filteredProducts;
}

// console.log(allProducts);
// console.log(
//   filterProductsByCategoryAndPrice(allProducts, "Electronics", 100, 400)
// );

function addDiscount(initialArray) {
  const productsWithDiscount = initialArray.map((product) => {
    if (product.quantity <= 5) {
      return { ...product, hasDiscount: true, price: product.price * 0.5 };
    } else {
      return product;
    }
  });
  return productsWithDiscount;
}

console.log("initial products", allProducts);
console.log("products with discount", addDiscount(allProducts));

function sumOfAllProducts(inititaArray) {
  const total = inititaArray.reduce(function (acc, curr) {
    return acc + curr.price;
  }, 0);
  return total;
}
console.log(sumOfAllProducts(allProducts));
