// .
// ^
// $

// +
// ?
// [0-9]
// {}
// |
// ()

// test()
// let emailRegEx = /^\S+@\S+\.\S+$/;
// let email1 = "user@gmail.com";
// let email2 = "usertest.com";

// console.log("email1", emailRegEx.test(email1)); //true
// console.log("email2", emailRegEx.test(email2)); //false

// match()
// let regEx = /\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,}\b/g;
// let text =
//   "You can see this emails: user@gmail.com, user1@gmail.com, usertest_gmail.com, final@gmail.com";
// let matches = text.match(regEx);
// console.log("matches", matches); // [user@gmail.com, user1@gmail.com, final@gmail.com]

// replace()
// string.replace(regexp|subStr, newSubStr|function)
// let text = "Welcome to the Kyiv sity";
// let result = text.replace("Kyiv", "Київ");
// console.log(text);
// console.log(result);

// let cost = "Яблука коштують 20 грн, а апельсини 30 грн";
// let result = cost.replace(/\d+/g, "50"); //[0-9]
// console.log(cost);
// console.log(result);

// let date = new Date();
// console.log(date);

// let dateWithMilliseconds = new Date(86400000); //86400000 - 1 day
// console.log(dateWithMilliseconds);

// let dateByDateStr = new Date("2023-09-30T15:00:00Z");
// let dateByDateStrTest = new Date("March 25, 2023 13:00:00");
// let dateByDateStrInvalid = new Date("ajdehgfjhsdfgs");
// console.log(dateByDateStr);
// console.log(dateByDateStrTest);
// console.log(dateByDateStrInvalid);

// new Date(year, month, date, hours, minutes, seconds, ms);
// get methods
// let date = new Date(2024, 1, 1, 20, 0, 59, 999);
// console.log(date);
// console.log("year", date.getFullYear());
// console.log("yearUTC", date.getUTCFullYear());
// console.log("month", date.getMonth());
// console.log("monthUTC", date.getUTCMonth());
// console.log("date", date.getDate());
// console.log("dateUTC", date.getUTCDate());
// console.log("day", date.getDay());
// console.log("dayUTC", date.getUTCDay());
// console.log("ms", date.getMilliseconds());
// console.log("msUTC", date.getUTCMilliseconds());

// set methods
// new Date().setFullYear(year!, month, date);
// new Date().setMonth(month!, date);
// new Date().setDate(date!);
// new Date().setHours(hours!, minutes, sec, ms);
// new Date().setMinutes(minutes!, sec, ms);
// new Date().setSeconds(sec!, ms);
// new Date().setMilliseconds(ms!);
// new Date().setTime(time);

// new Date().setUTCFullYear(year!, month, date);
// new Date().setUTCMonth(month!, date);
// new Date().setUTCDate(date!);
// new Date().setUTCHours(hours!, minutes, sec, ms);
// new Date().setUTCMinutes(minutes!, sec, ms);
// new Date().setUTCSeconds(sec!, ms);
// new Date().setUTCMilliseconds(ms!);
// new Date().setUTCTime(time);

// let date = new Date();
// let dateCopy = { ...date };
// console.log("Оригінальна дата:", date.toString()); //deep cody

// date.setFullYear(2023);
// date.setMonth(11);
// date.setDate(25);
// date.setHours(12);
// date.setMinutes(30);
// date.setSeconds(0);
// console.log("Змінена дата:", date.toString());

// let date = new Date();
// console.log("Сьогодні", date.toDateString());

// date.setDate(date.getDate() + 5);
// console.log("Через 5 днів", date.toDateString());

// toLocaleString()
let date = new Date();
console.log(date.toLocaleString());
console.log(date.toLocaleDateString());
